<?php
// Header type
header('Content-type: application/json');

// Variables
$name = @trim(stripslashes(ucwords($_POST['nome'])));
$email = @trim(stripslashes($_POST['email']));
$phone = @trim(stripslashes($_POST['telefone']));
$company = @trim(stripslashes(ucwords($_POST['empresa'])));
$brand = @trim(stripslashes($_POST['vc']));
$solution = @trim(stripslashes($_POST['solucao']));
$message = @trim(stripslashes($_POST['mensagem']));

$admin = $brand . ' <contato@vcone.com.br>';

// Messages
$error = 'Mensagem não enviada.';
$status = array(
  'type'=>'success',
  'message'=>'Mensagem enviada.'
);

// Notification
$subject_notification = 'Nova mensagem de: '.$name;
$notification = '<h2>Olá '.$brand.'</h2><p style="font-size: 16px">'.$name.' gostaria de falar sobre <b>'.$solution.'</b></p><i style="font-size: 16px">"'.nl2br($message).'"</i><p style="font-size: 16px">Entre em contato, aqui estão as informações:</p><br><div style="font-size: 16px"><b>Nome:</b> '.$name.'<br><b>E-mail:</b> '.$email.'<br><b>Telefone:</b> '.$phone.'<br><b>Empresa:</b> '.$company.'</div><br><br>---<br><br><p style="font-size: 16px"><b>'.$brand.'</b><br><small>É uma empresa VC ONE<br>http://vcone.com</small></p>';

$headers  = "From: ".$admin."\r\n";
$headers  = "Cco: Juliani Granzotti <juliani.granzotti@vcgroupweb.com>\r\n";
$headers .= "Reply-To: ".$name."\r\n"; 
$headers .= "Content-type: text/html; charset=utf-8\r\n";
$subject = $subject_notification;
$body = $notification;


// Autoresponder
$subject_autoresponder = 'A ' . $brand . ' recebeu a sua mensagem';
$autoresponder = '<h2>Olá '.$name.'</h2><p style="font-size: 16px">Nós recebemos a sua mensagem enviada pelo do nosso site.<br>Em breve entraremos em contato.<br><br><b>'.$brand.'</b><br><small>É uma empresa VC One<br>http://vcone.com</small></p>';

$headers_user  = "From: ".$admin."\r\n";
$headers_user .= "Reply-To: ".$admin."\r\n";
$headers_user .= "Content-type: text/html; charset=utf-8\r\n";
$subject_user = $subject_autoresponder;
$body_user = $autoresponder;

// Send email
$success = mail($admin, $subject, $body, $headers);

if(!$success) {
  $status = array(
      'type'=> 'danger',
      'message'=> $error
  );
} else {
	mail($email, $subject_user, $body_user, $headers_user);
}

echo json_encode($status);

die;