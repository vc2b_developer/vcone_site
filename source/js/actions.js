// Default Variables

var brand
var section
var Timer


(function($) {
  "use strict"; // Start of use strict
  var one = 1
  var position = 0

  // Setup smooth scrollbar
  var scrollbar = new Smooth({ 
    native: true, 
    preload: true, 
    ease: 0.1,
    callback: function(val) {
      check_if_in_view()
      scroll_stopped()
    } 
  })
  scrollbar.init()

  // Smooth scrolling using jQuery easing
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {

        scrollbar.scrollTo( target.offset().top - $('#navbar-top').outerHeight() )
        $('.dropdown-menu').collapse('hide');

        return false;
      }
    }
  });

  // Pointer
  $(document).mousemove(function(e){
    $('#pointer').css({
      top: e.pageY - 16,
      left: e.pageX,
    })
  })

  $('a').hover(function(){
    $("#pointer").fadeOut()
  }, function(){
    $("#pointer").fadeIn()
  })

  $('.btn').hover(function(){
    $("#pointer").fadeOut()
    $(this).parent().find('.btn:not(:hover)').css({
      'transition-duration': '300ms',
      'filter': 'opacity(0.35)'
    })
  }, function(){
    $('#pointer').fadeIn()
    $(this).parent().find('.btn').css({
      'filter': 'none'
    }).delay(500, function(){
      $(this).removeAttr('style')
    })
  })

  $('.dropdown-item').hover(function(){
    $("#pointer").fadeOut()
    $(this).parent().find('.dropdown-item:not(:hover)').css({
      opacity: 0.5,
      'filter': 'grayscale(1)'
      
    })
  }, function(){
    $('#pointer').fadeIn()
    $(this).parent().find('.dropdown-item').css({
      opacity: 1,
      'filter': 'grayscale(0)'
    }).delay(500, function(){
      $(this).removeAttr('style')
    })
  })

  // Modal Case
  $('#modalCase').on('show.bs.modal', function (event) {

    $('#overflow').css({
      'transform': 'translate3d(0, 0, 0)'
    })

    $("#preloader").delay(250).fadeIn()

    var button = $(event.relatedTarget)
    var id = button.data('id')
    var title = button.data('title')
    var logo = button.data('logo')
    var content = button.data('content')
    var testimonial = button.data('testimonial')
    var gallery = button.data('gallery')

    var modal = $(this)
    modal.css('visibility','hidden')

    if (testimonial) {
      // testimonial
      modal.find('.modal-body').find('.col-md-3, .col-md-9').hide()
      modal.find('.modal-body').addClass('pb-0')

      modal.find('.modal-body').find('.row').append(
        '<div class="testimonial-content"></div>' +
        '<figure class="testimonial-image"></figure>'
      )

      var name = testimonial.name
      var funcao = testimonial.function
      var message = testimonial.message

      modal.find('.testimonial-content').append(
        '<img src="images/icons/quotes.png" class="img-fluid d-none d-md-block mb-4 mx-auto" alt="">' + 
        '<img src="'+logo+'" class="img-fluid" alt="">' + 
        '<p class="message">'+message+'</p>' +
        '<p class="author"><span>'+name+'</span><br><small>'+funcao[0]+'<br>'+funcao[1]+'</small></p>'
      )

      modal.find('.testimonial-image').append(
        '<img src="'+gallery[0].url+'" class="img-fluid" alt="'+gallery[0].alt+'">'
      )

      modal.delay(5000).ready(function(){
        $("#preloader").fadeOut(function(){
          $("#overflow").css({
            'transform': 'translate3d(100%, 0, 0)'
          })
          modal.css('visibility','visible')
        })
      })

    } else {
      // carousel  
      modal.find('.modal-body').find('.col-md-3, .col-md-9').show()
      modal.find('.modal-body').removeClass('pb-0')

      modal.find('.modal-logo').attr('src', logo)
      modal.find('.modal-logo').attr('alt', id)
      modal.find('.modal-title').html(title)

      content.forEach(function(result){
        modal.find('.modal-text').append('<br>' + result + '<br>' )
      })

      gallery.forEach(function(result, index){
        if(index == 0) {
          var image = '<figure class="carousel-item mb-0 active"><img src="'+result.url+'" class="d-block w-100" alt="'+result.alt+'"><figcaption>'+result.alt+'</figcaption></figure>'
          var li = '<li data-target="#carouselCase" data-slide-to="'+index+'" class="active"><img src="'+result.url+'" class="d-block h-100" alt="'+result.alt+'"></li>'
        } else {
          var image = '<figure class="carousel-item mb-0"><img src="'+result.url+'" class="d-block w-100" alt="'+result.alt+'"><figcaption>'+result.alt+'</figcaption></figure>'
          var li = '<li data-target="#carouselCase" data-slide-to="'+index+'"><img src="'+result.url+'" class="d-block h-100" alt="'+result.alt+'"></li>'
        }

        modal.find('.carousel-inner').append(image)
        modal.find('.carousel-indicators').append(li)
      })

      modal.imagesLoaded().done( function( instance ) {
        // setTimeout(function(){
          $("#preloader").fadeOut(function(){
            $("#overflow").css({
              'transform': 'translate3d(100%, 0, 0)'
            })
            modal.css('visibility','visible')
          })
        // }, 2000)
      })
    }
  })

  $('#modalCase').on('hidden.bs.modal', function (event) {
    var modal = $(this)
    modal.find('.modal-logo').attr('src','')
    modal.find('.modal-logo').attr('alt','')
    modal.find('.modal-title').empty()
    modal.find('.modal-text').empty()
    modal.find('.carousel-inner').empty()
    modal.find('.carousel-indicators').empty()
    modal.find('.testimonial-content, .testimonial-image').remove()
  })

  // Toggle dropdown after click
  $('.dropdown-item a').on('click',function(e) {
    e.preventDefault();
    $('.dropdown-menu.show').dropdown('toggle');
  })

  // Autofill select
  $('body').on('change', 'select[name=vc]', function(e){
    var select = $(this)
    var item = select.val()

    var json = [
      {nome:"VC Business", solucoes:["Governança Corporativa", "Coaching Executivo", "Planejamento Financeiro", "Gestão Administrativa", "Comunicação Estratégica"]}, 
      {nome:"VC Concept", solucoes:["Projetos de Arquitetura", "Otimização de Espaços", "Design & Branding"]}, 
      {nome:"Next Dealer", solucoes:["Especialista em Construção", "Planejamento Financeiro de Obras", "Garantia de entrega no Cronograma", "Otimização de Recursos"]}, 
      {nome:"VC Digital", solucoes:["Realidade Virtual", "Realidade Aumentada", "Retail Tech e E-commerce", "Brand Content", "Plataformas"]}, 
      {nome:"VCI", solucoes:["Transformação Digital", "Internet of Things", "Artificial Intelligence", "Virtual Reality", "Blockchain"]}, 
      {nome:"VC Green", solucoes:["Car Care", "Sustentabilidade", "Economia Circular", "Certificação LEED", "Energia Fotovoltáica e Solar"]}
    ]

    $('select[name=solucao]').empty()
    $('select[name=solucao]').removeAttr('disabled')
    // $('label[for=solucao]').css({color: 'inherit'})

    $.each(json, function(index, value){
      if( value.nome === item ) { 
        $.each(value.solucoes, function(i, v){
          $('select[name=solucao]').append(
            '<option value="'+v+'">'+v+'</option>'
          )
        })
      }
    })

  })

  // Contact Forms
  $('#contact-form').submit(function(e){
    e.preventDefault()

    var form = $(this),
      url = './forms.php',
      data = {
        nome: form.find( "input#nome" ).val(),
        email: form.find( "input#email" ).val(),
        telefone: form.find( "input#telefone" ).val(),
        empresa: form.find( "input#empresa" ).val(),
        vc: form.find( "select#vc" ).val(),
        solucao: form.find( "select#solucao" ).val(),
        mensagem: form.find( "textarea#mensagem" ).val()
      }

    var posting = $.post(url, data)

    posting.done(function(result){
      $('#feedback').html("<div class='alert alert-"+result.type+" d-inline-block'>"+result.message+"</div>");
      setTimeout(function() {
        $('#feedback').fadeOut(function(){
          $(this).empty()
        })
      }, 3000);
    })
  })


})(jQuery); // End of use strict

