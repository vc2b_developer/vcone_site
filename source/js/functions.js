// Active current section
function init () {
  active_current_section()
  set_input_masks()
}

// Current section
function active_current_section() {
  $('section').each(function() {
    if($(this).offset().top >= $(window).scrollTop() && $(this).offset().top < $(window).scrollTop() + $(window).height()) {
      if ($(this).offset().top == $(window).scrollTop()) {
        section = $(this)
        id = $(this).attr('id').split('-')
      } else {
        section = $(this).prev()
        id = $(this).prev().attr('id').split('-')
      }

      sectionIn(section)
      brand = id[0]

      var image = $('#brand').find('img')
      var imgurl = 'images/logos/logo-'+brand+'.png'

      image.load(imgurl, function(response, status, xhr) {
        if (status == "error") {
          $(this).attr('src', 'images/logos/logo-vc_one.png');
        } else {
          $(this).attr('src', imgurl);
        }
      });
      
    } else {
      $('#brand').find('img').attr('src', 'images/logos/logo-vc_one.png')
    }
  });
}

// Section In
function sectionIn(section) {
  section.addClass('active')

  $('#overflow').css({
    'transform': 'translate3d(100%,0,0)'
  })

  $('#navbar-top').delay(1000).animate({
    top: 0,
    opacity: 1
  }, 1000, 'easeOutBounce')
}

// Toggle brand
function toggleBrand(id) {  
  var brand = id.split('-')[0]
  var imgurl = 'images/logos/logo-'+brand+'.png' 

  $(this).load(imgurl, function(response, status, xhr) {
    var src = status == "error" ? 'images/logos/logo-vc_one.png' : imgurl
    if($('#brand').find('img').attr('src') != src) {
      $('#brand').find('img').fadeOut('fast', function(){
        $(this).attr('src', src)
        $(this).fadeIn('fast')
      })
    }
  });
}

// Máscara de inputs
function set_input_masks(){ 
  $("input#telefone").mask('(99) 99999999?9').on("focusout", function () {
    var len = this.value.replace(/\D/g, '').length;
    $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
  });
}

// Check if in view
function check_if_in_view (first) {
  var $animation_elements = $('.animation-element');
  var $window = $(window);
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);
 
  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
      if($element.is('section') && $element.hasClass('active')) {
        toggleBrand($element.attr('id'))
      } 
    } else {
      $element.removeClass('in-view');
    }
  });
}

// Scroll is stopped   
function scroll_stopped(){ 
  var sections = document.querySelectorAll('section')

  $.each(sections, function(index, element){
    $(element).each(function() {

      if( $(this).offset().top >= $(window).scrollTop() && $(this).offset().top < $(window).scrollTop() + ($(window).height() * 0.5) ) {
        section = $(this)
        if(! section.hasClass('active')) {
          section.parent().find('.active').removeClass('active')
          section.addClass('active') 
          toggleBrand(section.attr('id'))           
        }
        
        if(section.hasClass('company')){
          setTimeout(function(){
            section.find('.background').addClass('zoom-in')
          }, 2000);
        } else {
          section.parent().find('.zoom-in').removeClass('zoom-in')
        }
      }
    });
  })
}
