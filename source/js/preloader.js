(function($) {
  "use strict"; // Start of use strict

  $('body').imagesLoaded()
  .always( function( instance ) {
    init()

    $('#preloader').fadeOut(function(){
      check_if_in_view(true)
    })
  })
  
})(jQuery) // End of use strict