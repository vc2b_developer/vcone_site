module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		// Compile
		pug: {
			compile: {
				options: {
					data: {
						debug: false,
						site: require('./source/site.json')
					},
				pretty: true
				},
				files: {
					'public/index.html': ['source/views/index.pug'],
					'public/default.html': ['source/views/default.pug']
				}
			}
		},

		htmlmin: {
			dist: {
				options: { 
					removeComments: true,
					collapseWhitespace: true
				},
				files: { 
					'public/index.html': 'public/index.html',
					'public/default.html': 'public/default.html'
				}
			}
		},

		watch: {
			files: ['source/scss/**', 'source/views/**', 'source/js/**'],
			tasks: ['pug', 'sass', 'autoprefixer', 'uglify', 'inline', 'htmlmin']
		},

		sass: {
			dev: {
				options: {
					style: 'compressed'
				},
				files: {
					'source/css/main.css': 'source/scss/main.scss',
					'source/css/critical.css': 'source/scss/critical.scss'
				}
			}
		},

		autoprefixer: {
			options: {
				cascade: true
			},

			critical: {
				src: 'source/css/critical.css',
				dest: 'public/css/critical.css'
			},

			main: {
				src: 'source/css/main.css',
				dest: 'public/css/main.css'
			},
		},

		uglify: {
			options: {
				mangle: false,
				compress: true
			},
			javascript: {
				options: {
					beautify: true
				},
				files: {
					'public/js/main.min.js': [
						'source/js/preloader.js', 
						'source/js/functions.js', 
						'source/js/actions.js'
					]
				}
			}
		},

		cwebp: {
      dynamic: {
        options: {
          q: 50
        },
        files: [{
          expand: true,
          cwd: 'source/images/', 
          // src: ['**/*.{png,jpg,gif}'],
          src: ['logos/logo-vc_one-default.png'],
          dest: 'public/images/'
        }]
      }
    },

		clean: {
			folder: ['public']
		},

		copy: {
			jquery: {
				expand: true, 
				flatten: true, 
				src: 'node_modules/jquery/dist/jquery.min.*',
				dest: 'public/js'
			},
			bootstrap: {
				expand: true, 
				flatten: true, 
				src: 'node_modules/bootstrap/dist/js/bootstrap.bundle.min.*',
				dest: 'public/js'
			},
			maskedinput: {
				expand: true, 
				flatten: true, 
				src: 'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
				dest: 'public/js'
			},
			easing: {
				expand: true, 
				flatten: true, 
				src: 'source/js/easing.*',
				dest: 'public/js',
			},
			imagesloaded: {
				expand: true, 
				flatten: true, 
				src: 'node_modules/imagesloaded/imagesloaded.pkgd.min.*',
				dest: 'public/js',
			},
			smoothscrolling: {
				expand: true, 
				flatten: true, 
				src: 'node_modules/smooth-scrolling/smooth-scrolling.*',
				dest: 'public/js',
			},
			images: {
				expand: true, 
				cwd: 'source/images/',
				src: '**',
				dest: 'public/images/'
			},
			forms: {
				expand: true, 
				cwd: 'source/php/',
				src: 'forms.php',
				dest: 'public/'
			},
			fonts: {
				expand: true, 
				cwd: 'source/fonts/',
				src: '**',
				dest: 'public/fonts/'
			}
		},

		inline: {
			dist: {
				src: 'public/default.html',
				dest: 'public/default.html'
			}
		},

		browserSync: {
			dev: {
				bsFiles: {
					src : [
						'public/css/*.css',
						'public/js/*.js',
						'public/*.html'
					]
				},
				options: {
					watchTask: true,
					server: './public'
				}
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-pug');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-cwebp');
	grunt.loadNpmTasks('grunt-inline');

	// Default task(s)
	grunt.registerTask('build', ['clean', 'pug', 'cwebp', 'copy', 'uglify', 'sass', 'autoprefixer', 'inline', 'htmlmin']);
	grunt.registerTask('default', ['build', 'browserSync', 'watch']);
	grunt.registerTask('serve', ['browserSync', 'watch']);

};